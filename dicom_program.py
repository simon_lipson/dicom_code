import os
import tarfile
import urllib
import pydicom
import glob
from shutil import copy
import pandas as pd
import click
from os.path import expanduser, join

FILES_PATH = expanduser('~/Desktop/DICOMS')


def download_dicoms(input_path):
    url = input_path

    file_tmp = urllib.request.urlretrieve(url, filename=None)[0]

    tar = tarfile.open(file_tmp)
    tar.extractall(FILES_PATH)


def structure_dicoms_in_hierarchy(input_path):
    download_dicoms(input_path)
    files = glob.glob(expanduser(join(FILES_PATH, '*.dcm')))
    for dicom_file in files:
        ds = pydicom.dcmread(dicom_file)
        directory_path = expanduser('~/Desktop/python_practice/{}/{}/{}'.format(ds.PatientName,
                                                                                ds.StudyInstanceUID,
                                                                                ds.SeriesInstanceUID))
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

        copy(dicom_file, '{}/'.format(directory_path))


def generate_list_of_patient_age_sex():
    files = glob.glob(expanduser(join(FILES_PATH, '*.dcm')))
    df = pd.DataFrame()
    for dicom_file in files:
        ds = pydicom.dcmread(dicom_file)
        df = pd.concat([df, pd.DataFrame({'name': [ds.PatientName],
                                          'age': [ds.PatientAge],
                                          'sex': [ds.PatientSex]})])
    return df.drop_duplicates()


def get_count_of_institutions():
    files = glob.glob(expanduser(join(FILES_PATH, '*.dcm')))
    institution_names = set()
    for dicom_file in files:
        ds = pydicom.dcmread(dicom_file)
        institution_names.add(ds.InstitutionName)

    return len(institution_names), institution_names


def get_average_CT_length():
    files = glob.glob(expanduser(join(FILES_PATH, '*.dcm')))
    df = pd.DataFrame()
    for dicom_file in files:
        ds = pydicom.dcmread(dicom_file)
        try:
            df = pd.concat([df, pd.DataFrame({'patient': [ds.PatientName],
                                             'series': [ds.SeriesInstanceUID],
                                             'start_time': [float(ds.InstanceCreationTime)]})])
        except AttributeError:
            pass

    df_max = df.groupby(['series'])['start_time'].max().reset_index()
    df_min = df.groupby(['series'])['start_time'].min().reset_index()
    merged_df = pd.merge(df_max, df_min, on='series')
    merged_df['ct_scan_duration'] = merged_df['start_time_x'] - merged_df['start_time_y']
    return (merged_df['ct_scan_duration'].mean())


@click.command()
@click.option('-i', '--input_path', required=True)
def run(input_path):
    structure_dicoms_in_hierarchy(input_path)
    print (generate_list_of_patient_age_sex())
    print ('The average CT scan took:')
    print (get_average_CT_length())
    print ('The amount of institutions is:')
    print (get_count_of_institutions())


if __name__ == '__main__':
    run()

